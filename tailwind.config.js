/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
	theme: {
		colors: {
			primary: 'var(--primary)',
			secondary: 'var(--secondary)',
			dark: 'var(--dark)',
			darker: 'var(--darker)',
			light: 'var(--light)',
			bright: 'var(--bright)',
			mediumLight: 'var(--medium-light)'

		}
	},
	variants: {},
	plugins: []
}
